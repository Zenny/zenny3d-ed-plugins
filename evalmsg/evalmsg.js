'use strict';

const Plugin = require('../plugin');

//let mdl = "selectChannel";
let channelid;
let guild;

function channel(id=channelid) {
	return findModule('getChannel').getChannel(id);
}

function members() {
	let mb = findModule('getMembers').getMembers(channel().guild_id);
	console.log(mb[0]);
	return mb;
}

function user(id) {
	let u = findModule('getUser').getUser(id);
	console.log(u);
	return u;
}

module.exports = new Plugin({
    name:'EvalMsg',
    description: 'Eval in your messages with ${2+2$}',
    author: 'Zenny3D',
    color: 'cyan',
    load () {
		monkeyPatch(findModule('selectChannel'), 'selectChannel', function() {
			 channelid = "";
			 guild = null;
			 //users = [];
			 channelid = arguments[0].methodArguments[1];
			 let ch = channel();
			 let gid = null;
			 if(ch != null)
			 	gid = ch.guild_id;
			 if(gid != null)
			 	guild = findModule('getGuild').getGuild(gid);
			console.log(channel());
			console.log(guild);
			arguments[0].callOriginalMethod(arguments[0].methodArguments[0], arguments[0].methodArguments[1]);
		});
      monkeyPatch( findModule('sendMessage'), 'sendMessage', function () {
            let input = arguments[0].methodArguments[1];
				const regex = /\${([\s\S]*?)\$}/gm;
				let m;
				let done = input;
				let matches = 0;
				while ((m = regex.exec(input.content)) !== null) {
				   // This is necessary to avoid infinite loops with zero-width matches
				   if (m.index === regex.lastIndex) {
				      regex.lastIndex++;
				   }
					let mt = m;
					matches++;
						setTimeout(()=>{
							try {
								done.content = done.content.replace(mt[0], eval(mt[1]));
							} catch(er) {console.error(er);}
						}, 0);
				}
				//console.log(arguments[0].methodArguments);
				setTimeout(()=> {
            	arguments[0].callOriginalMethod(arguments[0].methodArguments[0], done);
				}, 10*(matches/4));
      });
    },
    unload () {
        findModule('sendMessage').sendMessage.unpatch();
		  findModule('selectChannel').selectChannel.unpatch();
    }
});
