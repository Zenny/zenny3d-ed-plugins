# evalmsg

Evaluate JavaScript within your discord messages.

# How To Use

To create a block of JavaScript to be evaluated, use `${` to start the block and `$}` to end it. You can use anything JavaScript provides for you by default, as well as anything you can use in ED plugins and a few Discord helper utilities I listed below. You can have multiple tags in one message.

Examples:
```
5 + 5 = ${ 5+5 $}
Random: ${ Math.random() $}
${ 3+3 $} ${ 3+5 $} ${ 3+4 $}
```

**NOTES**

* Any valid JavaScript will work. Yes, that means vars and functions.
* You cannot pass a var or function from one tag to another.
* Keep in mind you are limited to the same message limit by default, these tags do not count as the future amount of characters they would be after evaluation.

# Special Stuff

There are some special functions in place to make it easier to interact with Discord a little bit.

* channel(id)
* channelid
* guild
* members()
* user(id)


**channel(id)**

The channel function gives info about the specified channel. If no id is passed (`channel()`) it will return info about the current channel. It includes fields like:

* guild_id
* id (The channel ID)
* nsfw
* name
* topic


**channelid**

Used to store the current channel ID. Not too useful otherwise.


**guild**

The guild variable holds info about the current guild. It includes fields like:

* afkChannelId
* afkTimeout
* name
* description
* icon
* id
* ownerId
* region
* roles
* joinedAt
* vanityURLCode
* banner

**NOTES**

* The channel and guild objects are printed to the console when you select a channel.
* NEITHER of them are populated with anything until you explicitly pick a channel at least once.
* For the above, Eval may fail and your message will not be fully processed if used immediately upon startup.


**members()**

Returns all **cached** members within the current guild as an array of member objects. It takes time to populate, and may only populate people you know as well as people visible in the member list on the right. To populate this list fully, scroll through the member list at least once before using. When called, the first member is printed to the console for reference. A member object has some of the following fields:

* nick
* roles
* userId


**user(id)**

To be used in conjunction with `members()` to get more details about a user. You can use `user(members()[0].userId)` for example to get a user object. When called, the user is printed to the console for reference. A user object has some of the following fields:

* bot
* avatarURL
* createdAt
* username
* usernameLowerCase
* discriminator
* tag (username and discriminator put together)
* id

**SPECIAL**

These fields seem to only be populated for the currently logged in user (You).

* email
* phone
