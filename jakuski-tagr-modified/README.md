# jakuski-tagr-modified

[evalmsg](https://gitlab.com/Zenny/zenny3d-ed-plugins/tree/master/evalmsg) isn't required for this, but there's no point in getting this version if you don't have it.

A modified version of jakuski's [tagr](https://github.com/jakuski/ed_plugins/tree/master/Tagr) that loops it back around to be processed by [evalmsg](https://gitlab.com/Zenny/zenny3d-ed-plugins/tree/master/evalmsg).

Add evalmsg tags with `${ $}`. SEE [evalmsg](https://gitlab.com/Zenny/zenny3d-ed-plugins/tree/master/evalmsg) DOCS FOR MORE DETAILS
